// Remember some divs for easy reference later.
var participantsDiv = document.getElementById('participants');
var whosTalkin = document.getElementById('whosTalkin');

// What ID has the highlight right now?
var currentHighlightedParticipantId = null;

function getDisplayNameFromId(id) {
  var participant = gapi.hangout.getParticipantById(id);
  if (!participant || !participant.hasAppEnabled) {
    return 'An unknown person';
  }
  return participant.person.displayName;
}

// Updates div to say who is shown in the video canvas
function updateDisplayedParticipant() {
  if (currentHighlightedParticipantId) {
    whosTalkin.innerHTML = '<p>Locked to ' +
        getDisplayNameFromId(currentHighlightedParticipantId) + '</p>';
    return;
  }

  var id = gapi.hangout.layout.getDefaultVideoFeed().getDisplayedParticipant();
  whosTalkin.innerHTML = '<p>Currently ' +
      getDisplayNameFromId(id) + '</p>';
}

// Shows a list of participants with clickable buttons to lock them
function updateParticipants() {
  var retVal = '<ul>';

  var participants = gapi.hangout.getEnabledParticipants();

  for (var index in participants) {
    var part = participants[index];

    retVal += '<li>' + getDisplayNameFromId(part.id) +
        ' <input type="button" value="Lock" id="lockMeButton"' +
            'onClick="lockParticipant(\'' + part.id + '\')"/>';
    retVal+='<input type="button" value="Silence" id="lockMeButton"' +
            'onClick="silenceParticipant(\'' + part.id + '\')"/>';
    retVal +='<input type="button" value="Remove" id="lockMeButton"' +
            'onClick="removeParticipant(\'' + part.id + '\')"/></li>';
  }
  retVal += '</ul>';

  // Check to see if the current participant
  if (currentHighlightedParticipantId &&
      !gapi.hangout.getParticipantById(currentHighlightedParticipantId)) {
    currentHighlightedParticipantId = null;
    showDefaultFeed();
  }

  participantsDiv.innerHTML = retVal;
  listUsersFeed();
}

// Displays the video feed that would normally be
// visible if the app.  The DefaultVideoFeed generally
// shows feeds based on their volume level.
function showDefaultFeed() {

  // Remove the highlighting.
  if (currentHighlightedParticipantId) {
    gapi.hangout.av.clearAvatar(currentHighlightedParticipantId);
  }

  currentHighlightedParticipantId = null;

  var feed = gapi.hangout.layout.getDefaultVideoFeed();
  var canvas = gapi.hangout.layout.getVideoCanvas();

  canvas.setVideoFeed(feed);
  canvas.setWidth(600);
  canvas.setPosition(300, 50);
  canvas.setVisible(true);

  // Update the text
  updateDisplayedParticipant();
}

// Displays the video feed for a given participant
function lockParticipant(partId) {

  // Remove any previous highlighting.
  if (currentHighlightedParticipantId) {
    gapi.hangout.av.clearAvatar(currentHighlightedParticipantId);
  }

  // Remember who is selected
  currentHighlightedParticipantId = partId;
  // Highlight this user with the red rectangle.
  gapi.hangout.av.setAvatar(currentHighlightedParticipantId,
      'http://mediakit001.appspot.com/static/images/participantHighlight.png');

  // Set the feed
  var feed = gapi.hangout.layout.createParticipantVideoFeed(partId);
  var canvas = gapi.hangout.layout.getVideoCanvas();

  canvas.setVideoFeed(feed);
  canvas.setWidth(600);
  canvas.setPosition(300, 50);
  canvas.setVisible(true);

  // Update the text
  updateDisplayedParticipant();
}

function silenceParticipant(partId) 
{
	if(gapi.hangout.av.isParticipantAudible(partId))
	{
		gapi.hangout.av.setParticipantAudible(partId,false);
	}
	else
	{
		gapi.hangout.av.setParticipantAudible(partId,true);
	}
	updateDisplayedParticipant();
}

function removeParticipant(partId) 
{
	if(gapi.hangout.av.isParticipantVisible(partId))
	{
		gapi.hangout.av.setParticipantVisible(partId,false);
	}
	else
	{
		gapi.hangout.av.setParticipantVisible(partId,true);
	}
	updateDisplayedParticipant();
	console.log("Remove");
}

function showParticipantsFeed() {

  var participantsArray = gapi.hangout.getParticipants();
  var incrementPos = 0;
  
  for (var index in participantsArray) {
    var partId = participantsArray[index];
    incrementPos+=50;
    displayParticipantsFeed(partId,incrementPos);
  }  
}

function displayParticipantsFeed(partId,incrementPos)
{

    var feed2 = gapi.hangout.layout.createParticipantVideoFeed(partId);
    var canvas2 = gapi.hangout.layout.getVideoCanvas();

    canvas2.setVideoFeed(feed2);
    canvas2.setWidth(300);
    canvas2.setPosition(300+incrementPos, 50);
    canvas2.setVisible(true);

}

function listUsersFeed()
{
	var participants = gapi.hangout.getEnabledParticipants();
	var left=300;
	var top=100;
	for (var index in participants) 
	{
		var part = participants[index];
		console.log(part);
		var feed = gapi.hangout.layout.createParticipantVideoFeed(part.id);
		var canvas = gapi.hangout.layout.getVideoCanvas();
		canvas.setVideoFeed(feed);
		canvas.setWidth(600);
		canvas.setPosition(left, top);
    	canvas.setVisible(true);
    	left=left+300;
    	top=top+50;
  }
}

// This runs when the gadget is ready
function init() {
  console.log('Init.');

  // When API is ready...
  gapi.hangout.onApiReady.add(
      function(eventObj) {
        if (eventObj.isApiReady) {
          console.log('API is ready');
          updateParticipants();
          updateDisplayedParticipant();
          //showParticipantsFeed();
          
          // Add some listeners

          // When people leave the hangout
          gapi.hangout.onParticipantsChanged.add(
              function(eventObj) {
                console.log('Participants changed');
                updateParticipants();
              });

          // Every time the default video feed changes, we want the text to change
          // Note that these events go off whether or not the default video feed
          // is currently displayed.
          gapi.hangout.layout.getDefaultVideoFeed().onDisplayedParticipantChanged.add(
              function(eventObj) {
                console.log('Displayed participant changed');
                updateDisplayedParticipant();
              });
         
        }
      });
}

// Wait for gadget to load.
gadgets.util.registerOnLoadHandler(init);
